package Lab3;

public class Author {
    private String name;
    private String email;
    private char gender;

    Author(String n, String e, char c)
    {
        this.name = n;
        this.email = e;
        this.gender = c;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public String getName() {
        return this.name;
    }

    public String getEmail() {
        return this.email;
    }

    public char getGender() {
        return this.gender;
    }

    @Override
    public String toString()
    {
        return "I'm " + this.name + " (" + this.gender + ") at " + this.email;
    }

    public static void main(String[] args) {
        Author a = new Author("Popa Razvan", "da@utcn.ro", 'm');
        System.out.println(a.toString());
        a.setName("Razvan");
        a.setEmail("nu@ubb.ro");
        System.out.println(a); //toString is called automatically when printing the object
    }
}
