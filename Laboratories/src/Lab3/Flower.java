package Lab3;

public class Flower{
    static int total_flowers;
    Flower(){
        total_flowers++;
    }

    public int getTotal_flowers() {
        return total_flowers;
    }

    public static void main(String[] args) {
        Flower[] garden = new Flower[5];
        for(int i =0;i<5;i++){
            Flower f = new Flower();
            garden[i] = f;
        }
        System.out.println(garden[1].getTotal_flowers());
    }
}