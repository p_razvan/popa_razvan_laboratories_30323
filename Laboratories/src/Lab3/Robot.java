package Lab3;

public class Robot {
    int x;
    Robot()
    {
        x = 1;
    }
    void change(int k)
    {
        if(k>=1)
        {
            this.x+=k;
        }
    }
    @Override
    public String toString()
    {
        return "x:" + this.x;
    }


    public static void main(String[] args) {
        Robot r = new Robot();
        System.out.println(r.toString());
        r.change(3);
        System.out.println(r.toString());
        r.change(-1);
        System.out.println(r.toString());
    }

}
