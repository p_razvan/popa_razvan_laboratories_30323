package Lab3;

import java.awt.*;

public class MyPoint {
    int x;
    int y;
    MyPoint()
    {
        this.x = 0;
        this.y = 0;
    }
    MyPoint(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getX() {
        return this.x;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int getY() {
        return this.y;
    }

    public void setXY(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString()
    {
        return "(" + this.x + "," + this.y + ")";
    }

    public double distance(int x, int y)
    {
        return Math.sqrt((Math.abs(this.x-x))*(Math.abs(this.x-x)) + (Math.abs(this.y-y))*(Math.abs(this.y-y)));
    }

    public double distance(MyPoint p)
    {
        return Math.sqrt((Math.abs(this.x-p.getX()))*(Math.abs(this.x-p.getX())) + (Math.abs(this.y-p.getY()))*(Math.abs(this.y-p.getY())));
    }

    public static void main(String[] args) {
        MyPoint p1 = new MyPoint();
        MyPoint p2 = new MyPoint(2,2);
        System.out.println(p1.toString());
        System.out.println(p2.toString());
        System.out.println(p1.distance(1,1));
        System.out.println(p1.distance(p2));
    }
}
