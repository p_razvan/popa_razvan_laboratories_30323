package Lab3;

public class Circle {
    private double radius = 1.0;
    private String color = "red";


    Circle(double r)
    {
        this.radius = r;
    }

    Circle(double r, String c)
    {
        this.radius = r;
        this.color = c;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea()
    {
        return this.radius*this.radius*Math.PI;
    }

    public static void main(String[] args) {
        Circle c1 = new Circle(1);
        Circle c2 = new Circle(4,"green");
        System.out.println(c1.getRadius());
        System.out.println(c2.getArea());
    }
}
