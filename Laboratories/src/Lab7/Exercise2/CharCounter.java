package Lab7.Exercise2;

import java.io.IOException;
import java.util.Scanner;

public class CharCounter {
    private static final String FILE_PATH = "C:\\Users\\Razvan\\Documents\\popa_razvan_laboratories_30323\\Laboratories\\src\\data.txt";
    public static void main(String args[]) throws IOException {
        FileHandler fileUtil = new FileHandler(FILE_PATH);
        System.out.println("ENTER CHAR TO COUNT:");
        Scanner sc = new Scanner(System.in);
        char c = sc.next().charAt(0);
        System.out.println();
        System.out.println("No. of characters in file: " + fileUtil.getCharCount(c));
    }

}
