package Lab7.Exercise2;

import java.io.*;

public class FileHandler {
    static BufferedReader reader = null;
    public FileHandler(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        reader = new BufferedReader(input);
    }

    public static int getCharCount(char c) throws IOException {
        int charCount = 0;
        String data;
        while((data = reader.readLine()) != null) {
            for(int i = 0; i < data.length(); i++) {
               if(data.charAt(i) == c)
               {
                   charCount ++;
               }
            }
        }
        return charCount;
    }
}
