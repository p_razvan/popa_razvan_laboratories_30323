package Lab7.Exercise1;
public class AmountException extends Exception{

    int n;
    public AmountException(int n, String msg){
        super(msg);
        this.n = n;
    }

    int getCoffeeNumber(){
        return n;
    }

}