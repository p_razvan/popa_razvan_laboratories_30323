package Lab7.Exercise1;

public class CoffeeMaker {
    private static int coffeeNumber = 0;
    private int maxCoffee = 10;

    Coffee makeCoffee() throws AmountException{
        coffeeNumber++;
        if(coffeeNumber > maxCoffee) throw new AmountException(coffeeNumber, "Too much coffee :(");
        System.out.println("Make a coffe");
        int t = (int)(Math.random()*100);
        int c = (int)(Math.random()*100);
        Coffee coffee = new Coffee(t,c);

        return coffee;
    }

    int getCoffeeNumber(){
        return coffeeNumber;
    }
}