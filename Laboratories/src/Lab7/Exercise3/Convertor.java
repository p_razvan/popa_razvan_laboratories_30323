package Lab7.Exercise3;
import java.io.IOException;
import java.util.Scanner;

public class Convertor {
    private static String FILE_PATH;
    public static void main(String args[]) throws IOException {
        System.out.println("ENTER FILEPATH:");
        Scanner sc = new Scanner(System.in);
        FILE_PATH = sc.nextLine();
        FileHandler fileHandler = new FileHandler(FILE_PATH);
        System.out.println();
        String aux = fileHandler.getData();
        String writeString ="";
        for(int i = 0; i < aux.length(); i++)
        {
            char c = aux.charAt(i);
            int val = (c) + 1;
            char wc = (char) val;
            writeString +=  String.valueOf(val);
        }
        System.out.println(writeString);
    }

}
