package Lab7.Exercise3;

import java.io.*;

public class FileHandler {
    static BufferedReader reader = null;
    public FileHandler(String filePath) throws FileNotFoundException {
        File file = new File(filePath);
        FileInputStream fileStream = new FileInputStream(file);
        InputStreamReader input = new InputStreamReader(fileStream);
        reader = new BufferedReader(input);
    }

    public static String getData() throws IOException {
        String file_data = "";
        String data;
        while((data = reader.readLine()) != null) {
            file_data += data;
        }
        return file_data;
    }
}
