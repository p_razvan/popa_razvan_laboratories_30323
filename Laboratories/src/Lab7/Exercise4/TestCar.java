package Lab7.Exercise4;

import java.io.*;
import java.util.Scanner;

public class TestCar {
    public static Scanner scan = new Scanner(System.in);

    public static void createCar(){
        String model; double price;
        System.out.print("Enter model of car: ");
        model = scan.next();
        System.out.print("Enter price of car: ");
        price = scan.nextDouble();
        Car car = new Car(model, price);
        carToFile(car);
    }
    public static void carToFile(Car car){//car.getModel()+".car"
        try(FileOutputStream fos = new FileOutputStream("src\\Lab7\\Exercise4\\CarFolder\\"+car.getModel()+".car")){
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(car);
        }catch (IOException e){
            e.printStackTrace();
        }
    }

    public static void findCars(){
        File carFolder = new File("src\\Lab7\\Exercise4\\CarFolder");
        File[] listOfCars = carFolder.listFiles();

        for(int i = 0; i < listOfCars.length; i++){
            if(listOfCars[i].isFile()) System.out.println(listOfCars[i].getName());
        }

    }

    public static void readCar(){
        String model;
        System.out.print("Enter model of car: ");
        model = scan.next();

        try(FileInputStream fis = new FileInputStream("src\\Lab7\\Exercise4\\CarFolder\\"+model+".car");
            ObjectInputStream ois = new ObjectInputStream(fis)){

            Car car = (Car) ois.readObject();
            System.out.println(car);
        }catch (IOException | ClassNotFoundException e){
            System.out.println("Object \"" + model + ".car\" not found.");
            e.printStackTrace();
        }
    }

    public static void main(String[] args) {
        Boolean sel = true;
        while(sel){
            System.out.println("What do you want to do?\nCreate car object(1) Show all car objects(2) Read a car object(3) Quit(4)");
            switch (scan.nextInt()){
                case 1:
                    createCar();
                    break;
                case 2:
                    findCars();
                    break;
                case 3:
                    readCar();
                    break;
                case 4:
                    sel = false;
                    break;
                default:
                    System.out.println("Not a valid option");
                    break;
            }
        }

    }
}