package Lab6.Exercise1;

import java.util.Objects;

public class BankAccount {
    private String owner;
    private double balance;

    public BankAccount(String name, double amount){
        this.owner = name;
        this.balance = amount;
    }

    public void withdraw(double amount){
        this.balance -= amount;
    }

    public void deposit(double amount){
        this.balance += amount;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public double getBalance() {
        return balance;
    }

    public String getOwner() {
        return owner;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && owner.equals(that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }

    public static void main(String[] args) {
        BankAccount b1 = new BankAccount("Da",10);
        BankAccount b2 = new BankAccount("Da",1);
        BankAccount b3 = new BankAccount("Da",10);

        System.out.println(b1.equals(b2));
        System.out.println(b1.equals(b3));
        System.out.println(b1.hashCode());
    }
}
