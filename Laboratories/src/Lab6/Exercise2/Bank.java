package Lab6.Exercise2;

import Lab6.Exercise1.BankAccount;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;

public class Bank{
    private ArrayList<BankAccount> accounts = new ArrayList<>();


    public void sortAccounts(int mode){
        if (mode == 0) {
            for (int i = 0; i < accounts.size() - 1; i++) {
                for (int j = i + 1; j < accounts.size(); j++) {
                    if (accounts.get(i).getBalance() >= accounts.get(j).getBalance()) {
                        Collections.swap(accounts, i, j);
                    }
                }
            }
        }
        else {
            for(int i = 0; i < accounts.size() - 1; i++){
                for (int j = i + 1; j < accounts.size(); j++){
                    if(accounts.get(i).getOwner().compareTo(accounts.get(j).getOwner()) == 1)
                    {
                        Collections.swap(accounts,i,j);
                    }
                }
            }
        }
    }

    public BankAccount getAccount(String owner){
        for (BankAccount b : accounts){
            if(b.getOwner() == owner){
                return b;
            }
        }
        return null;
    }

    public void addAccount(String name, double amount){
        BankAccount b = new BankAccount(name,amount);
        this.accounts.add(b);
    }

    public void printAccounts(){
        sortAccounts(0);
        for (BankAccount b :accounts){
            System.out.println(b.getOwner() + " " + b.getBalance());
        }
    }

    public void printAccounts(double minBalance, double maxBalance){
        sortAccounts(0);
        for (BankAccount b : accounts){
            if(b.getBalance()>=minBalance && b.getBalance()<= maxBalance){
                System.out.println(b.getOwner() + " " + b.getBalance());
            }
        }
    }



    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("da",20);
        bank.addAccount("daa",120);
        bank.addAccount("ada",50);
        bank.addAccount("dsa",60);
        bank.addAccount("nu",990);

        bank.printAccounts();
        System.out.println("---------------------------------------------");
        bank.printAccounts(50,200);
        System.out.println("---------------------------------------------");

        bank.sortAccounts(1);
        bank.printAccounts();
    }
}
