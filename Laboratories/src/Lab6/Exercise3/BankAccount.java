package Lab6.Exercise3;

import java.util.Objects;

public class BankAccount implements Comparable<BankAccount>{
    private String owner;
    private double balance;

    public BankAccount(String owner, double balance){
        this.owner = owner;
        this.balance = balance;
    }

    public void withdraw(double amount){
        if (balance >= amount) {
            balance = balance - amount;
        } else {
            balance = 0;
        }
    }
    public void deposit(double amount){
        balance = balance + amount;
    }

    public String getOwner() {
        return owner;
    }

    public double getBalance() {
        return balance;
    }

    @Override
    public int compareTo(BankAccount o){
        return Double.compare(balance, o.balance);
    }

    @Override
    public String toString() {
        return "{" +
                "owner:'" + owner + '\'' +
                ", balance:" + balance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BankAccount that = (BankAccount) o;
        return Double.compare(that.balance, balance) == 0 && Objects.equals(owner, that.owner);
    }

    @Override
    public int hashCode() {
        return Objects.hash(owner, balance);
    }
}