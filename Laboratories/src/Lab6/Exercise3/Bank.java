package Lab6.Exercise3;

import java.util.Iterator;
import java.util.TreeSet;

public class Bank {
    private TreeSet<BankAccount> accounts = new TreeSet<BankAccount>();

    public void addAccount(String owner, double balance){
        BankAccount s = new BankAccount(owner, balance);
        accounts.add(s);
    }

    public void printAccounts(){
        for (BankAccount account : accounts) System.out.println(account);
        System.out.println();
    }

    public void printAccounts(double minBalance, double maxBalance){
        Iterator<BankAccount> i = accounts.iterator();
        BankAccount ba;
        while(i.hasNext()) {
            ba = i.next();
            if(ba.getBalance() >= minBalance && ba.getBalance() <= maxBalance)
                System.out.println(ba);
        }
        System.out.println();
    }
    public BankAccount getAccount(String owner) {
        Iterator<BankAccount> i = accounts.iterator();
        BankAccount ba;
        while (i.hasNext()) {
            ba = i.next();
            if (ba.getOwner().equals(owner))
                return ba;
        }
        return null;
    }
    public TreeSet<BankAccount> getAllAccounts(){
        return accounts;
    }
}