package Lab6.Exercise3;

import java.util.Comparator;
import java.util.Iterator;
import java.util.TreeSet;

public class Main3 {
    public static void main(String[] args) {
        Bank bank = new Bank();
        bank.addAccount("da", 250);
        bank.addAccount("dada", 100);
        bank.addAccount("nu", 35);
        bank.addAccount("nunu", 10);
        bank.addAccount("nuda", 111);
        bank.addAccount("danu", 0);

        bank.printAccounts();
        bank.printAccounts(50, 250);

        BankAccount temp = bank.getAccount("dada");
        if(temp != null) System.out.println(temp);
        else System.out.println("Owner not found!");
        temp = bank.getAccount("yes");
        if(temp != null) System.out.println(temp);
        else System.out.println("Owner not found!");

        TreeSet<BankAccount> accounts = new TreeSet<BankAccount>(Comparator.comparing(BankAccount::getOwner));
        accounts.addAll(bank.getAllAccounts());
        Iterator<BankAccount> i = accounts.iterator();
        System.out.println("\nSorted alphabetically: ");
        while(i.hasNext())
            System.out.println(i.next());
    }
}