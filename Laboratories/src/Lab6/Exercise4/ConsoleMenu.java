package Lab6.Exercise4;

import java.util.Scanner;
import java.util.SortedMap;

public class ConsoleMenu {
    public static void main(String[] args) {
        Dictionary d = new Dictionary();
        d.addWord(new Word("da"), new Definition("adv. Cuvânt care se folosește pentru a răspunde afirmativ la o întrebare sau pentru a exprima o afirmație, un consimțământ."));
        d.addWord(new Word("dar"), new Definition("conj., adv. A. Conj. I. (Leagă propoziții sau părți de propoziție adversative) 1. (Arată o opoziție) Cu toate acestea, totuși. "));
        d.addWord(new Word("nu"),new Definition("conj., adv. A. Conj. I. (Leagă propoziții sau părți de propoziție adversative) 1. (Arată o opoziție) Cu toate acestea, totuși."));
        Scanner sc = new Scanner(System.in);
        String word;
        System.out.println("Options: 0)EXIT; 1)ADD WORD AND DEFINITION; 2)SEARCH WORD; 3)PRINT ALL WORDS; 4)PRINT ALL DEFINITIONS");
        int op = sc.nextInt();
        sc.nextLine();
        while(op!=0){
            switch (op) {
                case 0:
                    break;
                case 1:
                    System.out.println("ENTER WORD:");
                    word = sc.nextLine();
                    sc.nextLine();
                    System.out.println("ENTER DEFINITION:");
                    String definition = sc.nextLine();
                    d.addWord(new Word(word),new Definition(definition));
                    break;
                case 2:
                    System.out.println("ENTER WORD:");
                    word = sc.nextLine();
                    System.out.println(d.getDefinition(new Word(word)));
                    break;
                case 3:
                    System.out.println("ALL WORDS:");
                    d.getAllWords();
                    break;
                case 4:
                    System.out.println("ALL DEFINITIONS:");
                    d.getAllDefinitions();
                    break;
            }
            sc.nextLine();
            System.out.println("ENTER ANOTHER OPTION");
            op = sc.nextInt();
        }
    }
}
