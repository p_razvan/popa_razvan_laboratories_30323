package Lab6.Exercise4;

import java.util.HashMap;

public class Dictionary {
    private HashMap<Word, Definition> dictionary = new HashMap<>();

    public void addWord(Word w, Definition d){
        dictionary.put(w, d);
    }
    public Definition getDefinition(Word w){
        return dictionary.getOrDefault(w, null);
    }
    public void getAllWords(){
        System.out.println(dictionary.keySet());
    }
    public void getAllDefinitions(){
        System.out.println(dictionary.values());
    }
}