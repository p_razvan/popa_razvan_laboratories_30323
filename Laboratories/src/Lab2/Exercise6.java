package Lab2;

import java.util.Scanner;

public class Exercise6 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        //(a)
        int fact = 1;
        for(int i = 1; i <= N; i++)
        {
            fact *= i;
        }
        System.out.println(fact);
        //(b)
        System.out.print(factorial(N));
    }

    public static int factorial(int n)
    {
        if(n<=1)
        {
            return 1;
        }
        else
        {
            return n*factorial(n-1);
        }
    }
}
