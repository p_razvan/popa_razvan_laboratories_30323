package Lab2;

import java.util.Random;
import java.util.Scanner;

public class Exercise7 {
    public static void main(String[] args) {
        Random rnd = new Random();
        Scanner sc = new Scanner(System.in);
        int max = 100;
        int number = rnd.nextInt(0,max);
        System.out.println("Interval of guessing " + 0 + "-" + max);
        int num_guesses = 0;
        boolean win = false;
        while(num_guesses < 3 && !win)
        {
            int guess = sc.nextInt();
            if(guess < number)
            {
                System.out.println("Wrong answer, your number is too low");
                num_guesses ++;
            }
            else
            {
                if(guess > number)
                {
                    System.out.println("Wrong answer, your number it too high");
                    num_guesses ++;
                }
                else
                {
                    System.out.println("You win!");
                    break;
                }
            }
        }
        if(num_guesses == 3)
        {
            System.out.print("You lost!");
        }
    }
}
