package Lab2;

import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class Exercise5 {
    public static void main(String[] args) {
        Integer[] vector = new Integer[10];
        for(int i = 0; i < 10; i++) {
            Random rnd = new Random();
            vector[i] = rnd.nextInt(0,10);
            System.out.printf(vector[i] + " ");
        }
        System.out.println();
        boolean flag = true;
        for(int i = 0; i < 9; i++)
        {
            flag = false;
            for(int j = i + 1; j < 10; j++)
            {
                if(vector[i]> vector[j]) {
                    flag = true;
                    int aux = vector[i];
                    vector[i] = vector[j];
                    vector[j] = aux;
                }
            }
            if(!flag) break;
        }
        for(int i = 0; i < 10; i++) {
            System.out.print(vector[i] + " ");
        }
    }
}
