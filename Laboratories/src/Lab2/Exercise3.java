package Lab2;

import java.util.Scanner;

public class Exercise3 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int total = 0;
        for(int i = Math.min(a,b); i <= Math.max(a,b); i++)
        {
            if(prime(i))
            {
                System.out.print(i + " ");
                total++;
            }
        }
        System.out.println();
        System.out.print(total);
    }

    public static boolean prime(int a)
    {
        if(a <= 1)
        {
            return false;
        }
        int d = 2;
        while(d*d<=a)
        {
            if(a%d == 0)
            {
                return false;
            }
            d++;
        }
        return true;
    }
}
