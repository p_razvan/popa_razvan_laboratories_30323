package Lab2;

import java.lang.reflect.Array;
import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;
import java.util.Vector;

public class Exercise4 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int N = sc.nextInt();
        Integer[] vector = new Integer[N];
        for(int i = 0; i < N; i++) {
            Random rnd = new Random();
            vector[i] = rnd.nextInt(0,N);
        }
        int max = 0;
        for(int i = 0; i < N; i++)
        {
            if(vector[i] > max)
            {
                max = vector[i];
            }
        }
        System.out.println(max);
    }
}
