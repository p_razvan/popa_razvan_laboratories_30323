package Lab4.Exercise1;

public class Circle {
    private double radius = 1.0;
    private String color = "red";

    protected Circle(){}

    protected Circle(double radius)
    {
        this.radius = radius;
    }

    public double getRadius()
    {
        return radius;
    }

    public double getArea()
    {
        return getRadius()*getRadius()*Math.PI;
    }

    public static void main(String[] args) {
        Circle c1 = new Circle();
        Circle c2 = new Circle(4);
        System.out.println(c1.getArea());
        System.out.println(c2.getArea());
    }
}
