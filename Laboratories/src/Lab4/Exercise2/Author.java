package Lab4.Exercise2;

public class Author {
    private String name;
    private String email;
    private char gender;

    public Author(String name, String email, char gender)
    {
        this.name = name;
        this.email = email;
        this.gender = gender;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public char getGender() {
        return gender;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString()
    {
        return this.name + " (" + this.gender + ") at " + this.email;
    }

    public static void main(String[] args) {
        Author a1 = new Author("Popa Razvan", "razvi@utcn.ro",'m');
        Author a2 = new Author("Penzes Nowoemi", "noemi@uwutcn.ro", 'f');
        System.out.println(a1.toString());
        a1.setEmail("razvan@utcn.ro");
        System.out.println(a1.toString());
        System.out.println(a2.toString());
    }
}
