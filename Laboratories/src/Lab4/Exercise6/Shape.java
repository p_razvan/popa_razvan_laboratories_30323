package Lab4.Exercise6;

public class Shape {
    private String color ;
    private boolean filled;

    protected Shape(){

        this.color = "red";
        this.filled = true;

        this.color = "green";
        this.filled = true;

    }

    protected Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString()
    {
        if(isFilled())
            return "A shape with color of " + this.getColor() + " and filled";
        else
            return "A shape with color of " + this.getColor() + " and not filled";
    }
}
