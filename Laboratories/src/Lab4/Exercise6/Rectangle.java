package Lab4.Exercise6;

public class Rectangle extends Shape{
    private double width = 1.0;
    private double length = 1.0;

    protected Rectangle(){
        super();
    }

    protected Rectangle(double width, double length){
        super();
        this.width = width;
        this.length = length;
    }

    protected Rectangle(double width, double length, String color, boolean filled){
        super(color, filled);
        this.width = width;
        this.length = length;
    }

    public double getWidth() {
        return width;
    }

    public double getLength() {
        return length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getArea() {
        return this.getLength()*this.getWidth();
    }

    public double getPerimeter() {
        return 2*(this.getLength()+this.getWidth());
    }

    @Override
    public String toString()
    {
        return "A rectangle with width " + this.getWidth() + " and length "+ this.getLength() + " which is a sublcass of " + super.toString();
    }
}
