package Lab4.Exercise6;

public class Square extends Rectangle{
    public Square(){
        super();
    }

    public Square(double side)
    {
        super(side,side);
    }

    public Square(double side, String color, boolean filled){
        super(side,side,color,filled);
    }

    public double getSide() {
        return super.getLength();
    }

    public void setSide(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public void setLength(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public void setWidth(double side) {
        super.setLength(side);
        super.setWidth(side);
    }

    @Override
    public String toString() {
        return "A Square with side " + this.getSide() + " which is a subclass of " + super.toString();
    }

    public static void main(String[] args) {
        Square sq1 = new Square(1,"red",false);
        System.out.println(sq1.toString());
        Rectangle r1 = new Rectangle(2,1,"blue",true);
        System.out.println(r1.toString());
    }
}
