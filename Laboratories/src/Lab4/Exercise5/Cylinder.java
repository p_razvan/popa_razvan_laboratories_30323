package Lab4.Exercise5;

import Lab4.Exercise1.Circle;

public class Cylinder extends Circle {
    private double height = 1.0;

    public Cylinder(){
        super(); //to initialize the inherited attributes color and filled
    }

    public Cylinder(double radius) {
        super(radius);
    }

    public Cylinder(double radius, double height) {
        super(radius);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public double getVolume() {
        return super.getArea()*this.height;
    }
    //override the getArea method

    public static void main(String[] args) {
        Cylinder c1 = new Cylinder(1);
        Cylinder c2 = new Cylinder(1,3);
        System.out.println(c1.getVolume());
        System.out.println(c2.getVolume());
    }
}
