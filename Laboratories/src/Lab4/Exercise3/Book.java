package Lab4.Exercise3;

import Lab4.Exercise2.Author;

public class Book {
    private final String name;
    private final Author author;
    private double price;
    private int qtyInStock = 0;

    Book(String name, Author author, double price)
    {
        this.author = author;
        this.name = name;
        this.price = price;
    }

    Book(String name, Author author, double price, int qtyInStock)
    {
        this.author = author;
        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author getAuthor() {
        return author;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    @Override
    public String toString() {
        return this.name + " by " + this.author.toString();
    }

    public static void main(String[] args) {
        Author a1 = new Author("Ion Creanga","ion@utcn.ro",'m');
        Author a2 = new Author("Mihai Eminescu", "mihai@utcn.ro", 'm');
        Book b1 = new Book("Capra cu 3 yeezy",a1,20);
        Book b2 = new Book("Poezii",a2,25.40,10);
        System.out.println(b1.toString());
        System.out.println(b2.toString());
    }
}
