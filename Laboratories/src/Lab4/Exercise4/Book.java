package Lab4.Exercise4;

import Lab4.Exercise2.Author;

import java.util.Arrays;

public class Book {
    private String name;
    private Author[] authors;
    private double price;
    private int qtyInStock = 0;

    Book(String name, Author[] authors, double price)
    {
        this.authors = authors;
        this.name = name;
        this.price = price;
    }

    Book(String name, Author[] authors, double price, int qtyInStock)
    {
        this.authors = authors;
        this.name = name;
        this.price = price;
        this.qtyInStock = qtyInStock;
    }

    public String getName() {
        return name;
    }

    public Author[] getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQtyInStock() {
        return qtyInStock;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQtyInStock(int qtyInStock) {
        this.qtyInStock = qtyInStock;
    }

    public void printAuthors()
    {
        for (Author author : authors) {
            System.out.println(author.getName());
        }
    }

    @Override
    public String toString()
    {
        return this.name + " by " + this.authors.length;
    }

    public static void main(String[] args) {
        Author a1 = new Author("Ion Creanga","ion@utcn.ro",'m');
        Author a2 = new Author("Mihai Eminescu", "mihai@utcn.ro", 'm');
        Author a3 = new Author("Ion Luca Caragiale", "luca@utcn.ro",'m');
        Author[] authors = new Author[3];
        authors[0] = a1;
        authors[1] = a2;
        authors[2] = a3;
        Book b1 = new Book("Colectie clasa a 5 a",authors,20);
        Book b2 = new Book("Colectie clasa a 6 a",authors,25.40,10);
        System.out.println(b1.toString());
        b1.printAuthors();
        System.out.println(b2.toString());

    }
}
