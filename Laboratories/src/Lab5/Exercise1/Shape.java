package Lab5.Exercise1;

public abstract class Shape {
    protected String color ;
    protected boolean filled;

    protected Shape(){
        this.color = "green";
        this.filled = true;

    }

    protected Shape(String color, boolean filled) {
        this.color = color;
        this.filled = filled;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public boolean isFilled() {
        return filled;
    }

    public void setFilled(boolean filled) {
        this.filled = filled;
    }

    @Override
    public String toString()
    {
        if(isFilled())
            return "A shape with color of " + this.getColor() + " and filled";
        else
            return "A shape with color of " + this.getColor() + " and not filled";
    }

    abstract double getArea();
    abstract double getPerimeter();

}
