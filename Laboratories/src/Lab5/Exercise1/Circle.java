package Lab5.Exercise1;

public class Circle extends Shape {
    private double radius = 1.0;

    protected Circle(){
        super("green",true);
    }

    protected Circle(double radius){
        super();
        this.radius = radius;
    }

    protected Circle(double radius, String color, boolean filled){
        super(color, filled);
        this.radius = radius;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getArea()
    {
        return this.getRadius()*this.getRadius()*Math.PI;
    }

    public double getPerimeter()
    {
        return 2*this.getRadius()*Math.PI;
    }

    @Override
    public String toString()
    {
        return "A circle with radius " + this.getRadius() + "which is a sublcass of " + super.toString();
    }

}
