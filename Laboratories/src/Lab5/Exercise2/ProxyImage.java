package Lab5.Exercise2;

public class ProxyImage implements Image{

    private RealImage realImage;
    private RotatedImage rotatedImage;
    private String fileName;
    private boolean mode;

    public ProxyImage(String fileName, boolean mode){
        this.fileName = fileName;
        this.mode = mode;
    }

    @Override
    public void display() {
        if(realImage == null){
            realImage = new RealImage(fileName);
        }
        if(rotatedImage == null){
            rotatedImage = new RotatedImage(fileName);
        }
        if(mode)
        {
            realImage.display();
        }
        else {
            rotatedImage.display();
        }
    }
}