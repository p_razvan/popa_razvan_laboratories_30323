package Lab5.Exercise3;

public class Controller {
    private LightSensor lightSensor;
    private TemperatureSensor temperatureSensor;

    public void control()
    {
        for(int i = 0; i < 20; i++)
        {
            lightSensor = new LightSensor();
            temperatureSensor = new TemperatureSensor();
            System.out.println(lightSensor.readValue() + " " + temperatureSensor.readValue());
        }
    }

    public static void main(String[] args) {
        Controller c = new Controller();
        c.control();
    }
}
