package Lab5.Exercise4;

import java.util.Random;

public class LightSensor extends Sensor {

    private int value;
    @Override
    int readValue() {
        Random r = new Random();
        value = r.nextInt(0,100);
        return value;
    }
}
