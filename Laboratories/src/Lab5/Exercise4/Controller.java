package Lab5.Exercise4;

public class Controller {

    private static Controller controller;
    private LightSensor lightSensor;
    private TemperatureSensor temperatureSensor;


    private Controller() {
        this.lightSensor = new LightSensor();
        this.temperatureSensor = new TemperatureSensor();
    }

    public static Controller getController() {
        if (controller == null){
            controller = new Controller();
        }
        return controller;
    }
    public void control() {
        for (int i = 0; i < 20; i ++)
        {
            System.out.println(lightSensor.readValue());
            System.out.println(temperatureSensor.readValue());
        }

    }
    public static void main(String[] args) {
        Controller c = new Controller();
        c.control();
    }
}

