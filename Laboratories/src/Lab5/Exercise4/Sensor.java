package Lab5.Exercise4;

public abstract class Sensor {
    private String location;
    abstract int readValue();
    public String getLocation()
    {
        return location;
    }

}
